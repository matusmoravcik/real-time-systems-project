import java.io.DataInputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CommandListener implements Runnable {	
	private final DataInputStream input;
	private volatile boolean receiving;
	private ConcurrentLinkedQueue<Character> commands;
	
	public CommandListener(DataInputStream in)
	{
		if(in == null)
		{
			throw new IllegalArgumentException("DataInputStream cannot be null!");
		}
		this.input = in;
		this.receiving = true;
		this.commands = new ConcurrentLinkedQueue<Character>();
	}

	public ConcurrentLinkedQueue<Character> getCommands() {
		return commands;
	}

	public boolean isReceiving() {
		return receiving;
	}

	public void setReceiving(boolean receiving) {
		this.receiving = receiving;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		try
		{
			if(receiving)
			{
				System.out.println("LISTENING...");
			}
			while(receiving)
			{
				commands.add(input.readLine().charAt(0));				
			}			
		} catch (IOException e){
			e.printStackTrace();			
		}
		finally
		{
			if(receiving)
			{
				System.out.println("STOPPED LISTENING");
			}else{
				System.out.println("NOT LISTENING");
			}
			receiving = false;			
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}	
}
