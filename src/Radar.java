import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;

/**
 * Class used for operating and retrieving data from a simple radar. The Radar is composed of an EV3IRSensor, which can be rotated by an EV3MediumRegulatedMotor.
 * @author Matus Moravcik
 *
 */
public class Radar
{
	private final EV3IRSensor infraSensor;
	private final EV3MediumRegulatedMotor motor;
	private final EV3ColorSensor colorSensor;
	private final int rotationAngle;
	private volatile boolean run;
	private volatile boolean running;
	private volatile Reading lastReading;
	private volatile boolean centering;
	private volatile boolean forward;
	private ConcurrentMap<Integer, Float> proximityMap;
	
	/**
	 * Immutable class which can hold one measurement of the radar.
	 * @author Matus Moravcik
	 *
	 */
	public final static class Reading
	{
		private final int direction;
		private final float distance;
		
		/**
		 * @param dir direction the IR sensor is pointed at
		 * @param dist distance measurement from IR sensor
		 */
		Reading(int dir, float dist)
		{
			this.direction = dir;
			this.distance = dist;
		}
		
		public int getDirection()
		{
			return direction;
		}
		
		public float getDistance()
		{
			return distance;
		}
		
		@Override
		public String toString()
		{
			return direction + " " + distance + "\n";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + direction;
			result = prime * result + Float.floatToIntBits(distance);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Reading other = (Reading) obj;
			if (direction != other.direction)
				return false;
			if (Float.floatToIntBits(distance) != Float
					.floatToIntBits(other.distance))
				return false;
			return true;
		}
	};
	
	/**
	 * 
	 * @param infraSensor EV3IRSensor
	 * @param motor EV3MediumRegulatedMotor
	 * @param rotationAngle how much in degrees will radar try to rotate in each direction before it returns back
	 * @param motorSpeed sets speed of EV3MediumRegulatedMotor
	 */
	public Radar(EV3IRSensor infraSensor, EV3MediumRegulatedMotor motor, int rotationAngle, EV3ColorSensor colorSensor)
	{
		if(infraSensor == null || motor == null)
		{
			throw new IllegalArgumentException("Hardware access class cannot be null.");
		}
		this.infraSensor = infraSensor;
		this.motor = motor;			
		this.rotationAngle = rotationAngle;		
		this.colorSensor = colorSensor;		
		
		this.run = false;	
		this.running = false;
		
		centering = false;
		forward = true;
		
		//assuming danger everywhere before first reading & preventing null pointer exceptions
		lastReading = new Reading(0, 0);
		proximityMap = new ConcurrentHashMap<>();
		for(int degree = 0; degree < 360; ++degree)
		{
			proximityMap.put(degree, 0.0f);
		}
	}
	
	public Radar(EV3IRSensor infraSensor, EV3MediumRegulatedMotor motor, int rotationAngle)
	{
		this(infraSensor, motor, rotationAngle, null);
	}
	
	public Radar(EV3IRSensor infraSensor, EV3MediumRegulatedMotor motor, EV3ColorSensor colorSensor)
	{
		this(infraSensor, motor, 180, colorSensor);		
	}	
	
	public Radar(EV3IRSensor infraSensor, EV3MediumRegulatedMotor motor)
	{
		this(infraSensor, motor, 180, null);		
	}

	/**
	 * Main output.
	 * @return Latest measurement of this radar.
	 */
	public Reading getLastReading()
	{
		return lastReading;
	}
	
	public HashMap<Integer, Float> getProximityMap()
	{
		return new HashMap<Integer, Float>(proximityMap);
	}
	
	public int getRotationAngle()
	{
		return rotationAngle;
	}

	/**
	 * 
	 * @return true iff radar is currently taking measurements
	 */
	public boolean isRunning()
	{
		return running;
	}
	
	/**
	 * Stops radar from rotating and taking measurements. May be resumed with Radar.run(); 
	 */
	public void stop()
	{
		run = false;
	}
	
	public void stopAndWait()
	{
		run = false;
		while(isRunning()) { }	
	}
	
	/**
	 * This method makes the radar to execute its function in another thread.
	 */
	public void run()
	{
		while(isRunning()) { }
		
		//initialization	      
		running = true;	        	
    	run = true;
		new Thread(new Runnable()
	    {
	        @Override
	        public void run()
	        {	        	
	        	SampleProvider distanceProvider = infraSensor.getMode("Distance");		
	    		float[] distanceSample = new float[distanceProvider.sampleSize()];
	    		
	    		if(forward)
	    		{
	    			motor.forward();
	    		}else{
	    			motor.backward();
	    		}
	    		
	    		//main loop
	    		while(run)
	    		{
	    			//measuring
	    			distanceProvider.fetchSample(distanceSample, 0);	
	    			int dir = motor.getTachoCount();	    			
	    			
	    			//movement control
	    			if((Math.abs(dir) >= rotationAngle && centering == false) || motor.isStalled())
	    			{
	    				centering = true;
	    				forward = !forward;
	    				if(forward)
	    				{
	    					motor.forward();	    					
	    				}
	    				else
	    				{
	    					motor.backward();	    					
	    				}
	    			}
	    			if(centering && Math.abs(dir) <= rotationAngle / 2)
	    			{
	    				centering = false;
	    			}
	    			
	    			//distance normalization
	    			if(distanceSample[0] == Float.POSITIVE_INFINITY)
	    			{
	    				distanceSample[0] = 1;
	    			}else{
	    				distanceSample[0] /= 60;
	    			}
	    			
	    			//direction normalization    			    			
	    			if(dir < 0)
	    			{
	    				dir = 360 - ((-dir) % 360);
	    			}else{
	    				dir %= 360;
	    			}
	    			
	    			//updating results
	    			fillProximityMapHiatus(lastReading.getDirection(), lastReading.getDistance(), dir, distanceSample[0]);
	    			proximityMap.replace(dir, distanceSample[0]);
	    			lastReading = new Reading(dir, distanceSample[0]);
	    		}
	    		//finalization
	    		motor.stop();
	    		running = false;
	        }
	    }).start();
	}	
	
	/**
	 * Fills the shortest section of proximity map between dir1 and dir2 with data starting with value of dist1 and linearly progressing to value of dist2.	 
	 */
	private void fillProximityMapHiatus(int dir1, float dist1, int dir2, float dist2)
	{
		int dirDiff = Math.abs(dir1 - dir2);
		if(dirDiff > 360 - dirDiff)
		{
			if(dir1 < dir2)
			{
				dir1 += 360;
			}else{
				dir2 += 360;
			}
		}
		if(Math.abs(dir1 - dir2) < 2)
		{
			return;
		}
		if(dir1 > dir2)
		{
			int x = dir1;
			dir1 = dir2;
			dir2 = x;
			
			float y = dist1;
			dist1 = dist2;
			dist2 = y;
		}
		float dirHiatus = dir2 - dir1;		
		
		float distanceDiff = dist2 - dist1;
		for(int i = dir1 + 1; i < dir2; ++i)
		{
			proximityMap.replace(i % 360, dist1 + distanceDiff * ((i - dir1) / dirHiatus));
		}		
	}
	
	/**
	 * first inclusive, last exclusive
	 * @param dir1
	 * @param dir2
	 * @return
	 */
	public static LinkedList<Integer> getSmallerSegment(int dir1, int dir2)
	{
		int dirDiff = Math.abs(dir1 - dir2);		
		if(dirDiff > 360 - dirDiff)
		{
			if(dir1 < dir2)
			{
				dir1 += 360;
			}else{
				dir2 += 360;
			}
		}
		LinkedList<Integer> result = new LinkedList<>();
		if(Math.abs(dir1 - dir2) < 1)
		{
			return result;
		}
		if(dir1 > dir2)
		{
			int x = dir1;
			dir1 = dir2;
			dir2 = x;
		}
		for(int angle = dir1; angle < dir2; ++angle)
		{
			result.add(angle % 360);			
		}
		return result;
	}
	
	/**
	 * Discover limitations of rotation and move between them.
	 * @return
	 */
	public int centerBetweenLimits()
	{
		stopAndWait();
		running = true;
		
		int originalSpeed = motor.getSpeed();
		motor.setSpeed(200);
		
		motor.forward();
		while(!motor.isStalled()) { }
		int tachoCount1 = motor.getTachoCount();
		
		motor.backward();
		while(!motor.isStalled()) { }
		motor.stop();		
		int tachoCount2 = motor.getTachoCount();
		
		int result = (tachoCount1 - tachoCount2) / 2;
		motor.rotate(result);
				
		motor.setSpeed(originalSpeed);
		running = false;
		return result;
	}
	
	/**
	 * Find the black color mark with colorSensor.
	 * @return
	 */
	public int findColorMark()
	{
		if(colorSensor == null)
		{
			return -1;
		}
		stopAndWait();
		running = true;
		
		int originalSpeed = motor.getSpeed();
		motor.setSpeed(100);
		int step = 40;
		boolean center = false;
		boolean fwd = true;
		motor.forward();
		while(colorSensor.getColorID() != Color.BLACK && step <= 220)
		{
			int tachoCount = motor.getTachoCount();
			if((Math.abs(tachoCount) >= step && center == false) || motor.isStalled())
			{
				step += 40;
				center = true;
				fwd = !fwd;
				if(fwd)
				{
					motor.forward();	    					
				}
				else
				{
					motor.backward();	    					
				}
			}
			if(center && Math.abs(tachoCount) <= step / 2)
			{
				center = false;
			}
		}		
		motor.stop();
		motor.setSpeed(originalSpeed);
		running = false;
		if(step > 220)
		{
			return -2;
		}		
		return 0;
	}
	
	public void resetState()
	{
		centering = false;
		forward = true;	
		motor.resetTachoCount();
	}

	@Override
	public String toString() {
		return "Radar [infraSensor=" + infraSensor + ", motor=" + motor
				+ ", run=" + run + ", running=" + running + ", rotationAngle=" + rotationAngle
				+ ", lastReading=" + lastReading + "]";
	}	
}
