import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;

import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.navigation.DifferentialPilot;

public class MainClass{

	public static void main(String[] args)
	{		
		//radar setup
		EV3IRSensor infraSensor = new EV3IRSensor(SensorPort.S3);
		EV3MediumRegulatedMotor motor = new EV3MediumRegulatedMotor(MotorPort.D);			
		EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S4);
		motor.setSpeed(300);
		Radar radar = new Radar(infraSensor, motor, 220, colorSensor);	
		
		//navigation setup
		NavigationControl navigationController = new NavigationControl(new DifferentialPilot(3.25f, 18.5f, new EV3LargeRegulatedMotor(MotorPort.A), new EV3LargeRegulatedMotor(MotorPort.C)));		
		
		//bluetooth connection setup
		System.out.println("CONNECTING...");
		ServerSocket serv = null;
		DataOutputStream out = null;
		DataInputStream in = null;
		try {
			serv = new ServerSocket(12345);
			Socket s = serv.accept(); // waiting for connection	
			
			out = new DataOutputStream(s.getOutputStream());
			out.flush();
			out.writeUTF("HELLO");
			
			in = new DataInputStream(s.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		CommandListener cmdListener = new CommandListener(in);
		Thread listenerThread = new Thread(cmdListener);
		listenerThread.start();
		
		//work
		radar.run();	
		navigationController.run();
		System.out.println("RUNNING");
		int previousDir = -1;
		boolean flattenedMode = false;
		boolean run = true;
		boolean paused = false;
		while(Button.ESCAPE.isUp() && run)
	    {
			Radar.Reading reading = radar.getLastReading();
			try
			{
				if(!paused && reading.getDirection() != previousDir)
				{
					if(radar.isRunning())
					{
						if(flattenedMode) //send segment missed by radar between previous and last direction in correct order
						{
							HashMap<Integer, Float> proximityMap = radar.getProximityMap();
							LinkedList<Integer> missedSegment = Radar.getSmallerSegment(previousDir, reading.getDirection());
							if(previousDir == missedSegment.removeFirst()) //affects direction - if true, radar is rotating forward
							{
								while(missedSegment.isEmpty() == false)
								{
									int direction = missedSegment.removeFirst();
									out.writeUTF(new Radar.Reading(direction, proximityMap.get(direction)).toString());
								}
							}else{
								while(missedSegment.isEmpty() == false)
								{
									int direction = missedSegment.removeLast();
									out.writeUTF(new Radar.Reading(direction, proximityMap.get(direction)).toString());
								}
							}						
						}
						out.writeUTF(reading.toString());
					}
					previousDir = reading.getDirection();
				}
				
				char command = '.';
				if(cmdListener.getCommands().isEmpty() == false)
				{
					command = cmdListener.getCommands().poll();
					System.out.println(command);//
				}				
				switch(command)
				{
					case 'W': //navigator command
					case 'S': //navigator command
					case 'A': //navigator command
					case 'D': //navigator command
					case 'I': //navigator command
					case 'K': //navigator command
					case 'J': //navigator command
					case 'L': //navigator command
					case '0': //navigator command
					case '1': //navigator command
					case '2': //navigator command
					case '3': //navigator command
					{
						navigationController.getCommands().add(command);
						break;
					}
					case 'E': //escape
					{
						System.out.println("ESCAPING...");
						int dir = findEscape(radar.getProximityMap());
						if(dir == -1)
						{
							Sound.beep();
							System.out.println("NO ESCAPE");
							break;
						}
						System.out.println("ESCAPE: " + dir);
						char rot30cmd;
						char rot10cmd;
						if(dir < 180)
						{
							rot30cmd = 'D';
							rot10cmd = '3';
						}else{
							rot30cmd = 'A';
							rot10cmd = '1';
							dir -= 180;
						}
						
						int rotate30 = (int) Math.floor(dir / 30);						
						for(int i = 0; i < rotate30; ++i)
						{
							navigationController.getCommands().add(rot30cmd);
						}
						
						int rotate10 = (int) Math.round((dir - rotate30 * 30) / 10);
						for(int i = 0; i < rotate10; ++i)
						{
							navigationController.getCommands().add(rot10cmd);
						}
						
						navigationController.getCommands().add('M');
						break;
					}
					case 'P': //pause/resume
					{
						if(paused == false)
						{
							radar.stopAndWait();
							motor.flt();//
							navigationController.stopAndWait();
							paused = true;
							System.out.println("PAUSED");
						}else{
							System.out.println("RESUMING...");
							radar.run();
							navigationController.run();
							paused = false;
						}
						break;
					}
					case 'R': //send radar proximity map
					{
						System.out.println("SENDING MAP...");
						HashMap<Integer, Float> proximityMap = radar.getProximityMap();
						for(int angle = 0; angle < 360; ++angle)
						{
							out.writeUTF(angle + " " + proximityMap.get(angle).toString() + "\n");
						}
						break;
					}
					case 'F': //flattened mode trigger
					{
						flattenedMode = !flattenedMode;
						break;
					}
					case 'Z': //slow down the radar
					{
						radar.stopAndWait();
						motor.setSpeed(Math.max(80, motor.getSpeed() - 40));
						radar.run();
						break;
					}
					case 'X': //speed up the radar
					{
						radar.stopAndWait();
						motor.setSpeed(Math.min(600, motor.getSpeed() + 40));
						radar.run();
						break;
					}
					case 'C': //calibrate
					{
						System.out.println("CALIBRATING...");
						radar.centerBetweenLimits();
						radar.findColorMark();
						radar.resetState();
						radar.run();						
						break;
						}
					case 'Q': //quit program
					{
						System.out.println("QUITTING...");
						run = false;
						break;
					}
					default:
					{
						break;
					}					
				}	
			} catch (IOException e) {
				e.printStackTrace();				
			}
	    }
		
		//finalization
		radar.stopAndWait();
		navigationController.stopAndWait();	
		
		infraSensor.close();
		motor.close();
		if(colorSensor != null)
		{
			colorSensor.close();
		}		
		if(out != null)
		{
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
		if(serv != null)
		{
			try {
				serv.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
	}
	
	public static int findEscape(HashMap<Integer, Float> proximityMap)
	{
		int firstObstacle = 0;
		for(int angle = 0; angle < 360; ++angle)
		{
			if(proximityMap.get(angle) < 1.0)
			{
				firstObstacle = angle;
				break;
			}
		}
		
		boolean object = true;
		int result = 0;		
		int maxWidth = 0;
		int startPos = firstObstacle;		
		for(int currentPos = firstObstacle; currentPos < firstObstacle + 360; ++currentPos)
		{
			if(object == false && proximityMap.get(currentPos % 360) < 1.0)
			{
				int width = Math.abs(startPos - currentPos);
				if(width > maxWidth)
				{
					maxWidth = width;
					result = (startPos + currentPos) / 2;
				}
				object = true;
				continue;
			}
			if(object && proximityMap.get(currentPos % 360) >= 1.0)
			{
				startPos = currentPos;
				object = false;
			}			
		}
		int lastPos = firstObstacle + 359;
		if(object == false)
		{
			int width = Math.abs(startPos - lastPos);
			if(width > maxWidth)
			{
				maxWidth = width;
				result = (startPos + lastPos) / 2;
			}
		}
		if(maxWidth < 60)
		{
			return -1;
		}
		return result % 360;
	}
}
