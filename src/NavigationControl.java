import java.util.concurrent.ConcurrentLinkedQueue;

import lejos.robotics.navigation.DifferentialPilot;

public class NavigationControl {
	private final DifferentialPilot pilot;
	private ConcurrentLinkedQueue<Character> commands;
	private volatile boolean running;
	private volatile boolean run;
	
	public NavigationControl(DifferentialPilot differentialPilot)
	{
		if(differentialPilot == null)
		{
			throw new IllegalArgumentException("Pilot cannot be null.");
		}
		this.pilot = differentialPilot;		
		running = false;
		this.commands = new ConcurrentLinkedQueue<Character>();
	}

	public ConcurrentLinkedQueue<Character> getCommands()
	{
		return commands;
	}

	public boolean isRunning()
	{
		return running;
	}
	
	public void stop()
	{
		run = false;		
	}
	
	public void stopAndWait()
	{
		run = false;
		while(isRunning()) { }
	}

	public void run() {
		running = true;
		run = true;		
		new Thread(new Runnable()
	    {
	        @Override
	        public void run()
	        {	
	        	while(run)
	    		{
	        		char command = '.';
	        		if(commands.isEmpty() == false)
	        		{
	        			command = commands.poll();
	        		}
	        		switch(command)
					{
						case 'W': //forward
						{
							pilot.travel(20);
							break; 
						}
						case 'S': //backward
						{
							pilot.travel(-20);
							break; 
						}
						case 'A': //turn left
						{
							pilot.rotate(30.0f);
							break;
						}
						case 'D': //turn right
						{
							pilot.rotate(-30.0f);
							break;
						}
						case '2': //decent forward
						{
							pilot.travel(5);
							break; 
						}						
						case '1': //decent turn left
						{
							pilot.rotate(10.0f);
							break;
						}
						case '3': //decent turn right
						{
							pilot.rotate(-10.0f);
							break;
						}
						case '0': //stop
						{
							pilot.stop();
							break;
						}
						case 'I': //continuous forward
						{
							pilot.forward();
							break;
						}
						case 'K': //continuous backward
						{
							pilot.backward();
							break;
						}
						case 'J': //continuous left
						{
							pilot.rotateLeft();
							break;
						}
						case 'L': //continuous right
						{
							pilot.rotateRight();
							break;
						}
						case 'M': //forward 30cm
						{
							pilot.travel(30);
							break;
						}
						default:
						{
							break;
						}					
					}	
	    		}
	    		//finalization
	    		pilot.stop();
	    		running = false;
	        }
	    }).start();		
	}
}
